import requests
url = "https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv"


with open('iris.csv', 'wb') as out_file:
    content = requests.get(url, stream=True).content
    out_file.write(content)

