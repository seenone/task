from app import  max_sepal_length, describe
 


def test_max_sepal_length_low():
    assert max_sepal_length(3) == '{"setosa":50,"versicolor":50,"virginica":50}'
    
def test_max_sepal_length_middle(): 
    assert max_sepal_length(5.2) == '{"setosa":11,"versicolor":45,"virginica":49}'

def test_max_sepal_length_high(): 
    assert max_sepal_length(7) == '{"virginica":12}'



