from app import generete_sequence, find_longest


def test_generate_sequence_even_number():
    assert generete_sequence(8) == [4, 2, 1]


def test_generate_sequence_odd_number():
    assert generete_sequence(21) == [64,32,16,8,4,2,1]


def test_find_longest():
    assert find_longest(21) == "18"



