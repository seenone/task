from flask import Flask, jsonify
import pandas as pd

app = Flask(__name__)

df = pd.read_csv("iris.csv")

def generete_sequence(n):
    n = int(n)

    sequence = []

    while n != 1:
        print(n)
        if n % 2 == 0:
            n = n // 2 
        else:
            n = 3*n + 1
        sequence.append(n)

    return sequence


def find_longest(n):
    n = int(n)
    max_len = 0
    max_item = -1

    for i in range(1, n):
        length = len(generete_sequence(i))
        if length > max_len:
            max_len = length
            max_item = i
   
    return str(max_item)


@app.route('/sequence/elem/<n>')
def sequence_view(n):
    sequence = generete_sequence(n)
    return jsonify(", ".join([str(element) for element in sequence]))


@app.route('/sequence/longest/<n>')
def longest_element(n):
    return jsonify(find_longest(n))


@app.route('/iris/group/sepal_length/<max>')
def max_sepal_length(max):
    max = float(max)
    return df[df.sepal_length > max].groupby(["species"]).count().iloc[:,1].to_json()



@app.route('/iris/describe')
def describe():
    return  df.describe().to_html(classes="table table-striped")